@isTest
private class TestAvailableTrigger {
    @isTest static void testCheckTrue() {
        Integer r = 10 ;
        Date d = Date.newInstance(2020, 2, 17);
        List<Product__c> newprod = new List<Product__c>();
        for(Integer i=0; i<r; i++){
            Product__c a = new Product__c(Name='Test' + i, Price__c =5+i, Release_Date__c = d,
                                          Amount__c = 6);
            newprod.add(a);
        }
        Test.startTest();
        Database.SaveResult[] result = Database.insert(newprod, false);
        Test.stopTest();
        for(Database.SaveResult ir : result) {
            System.assert(ir.isSuccess());
        }
    }
    
    @isTest static void testCheckFalse() {
        Date d = Date.newInstance(2020, 2, 17);
        Product__c b = new Product__c(Name='Test', Price__c =5, Release_Date__c = d, Amount__c = 5);
        insert b;
        b.Amount__c = 0;
        Test.startTest();
        Database.SaveResult result = Database.update(b, false);
        Test.stopTest();
        System.assert(result.isSuccess());        
    }
}