public class ProductTablePageController {
    public String sortOrder = 'CreatedDate';
    public String ascOrDesc = ' ASC ';
    public Product__c prod { get; set; }
    public Boolean rend { get; set; }
    public Boolean rend2 { get; set; }
    public String searchInput { get; set;}
    public Product__c updatedProduct { get; set; }
    public String plId { get; set; }
    public String searchtext = null;
    public List<Product__c> prdc;
    private Integer totalRecs = 0;
    private Integer OffsetSize = 0;
    private Integer LimitSize;
    
    public ProductTablePageController(){
        rend = false;
        rend2 = false;
        totalRecs = [SELECT count() FROM Product__c];
        LimitSize = 5;
    }  
    
    public List<Product__c> getProducts() {
        
        if (searchtext != null){
            List<Product__c> results = Database.query('SELECT Name, Id,  Price__c, Amount__c, Type__c, Added_Date__c,Release_Date__c, Available__c '+
                                                      'FROM Product__c'+' WHERE Name LIKE \'%' + searchInput + '%\' ORDER BY '+
                                                      sortOrder + ascOrDesc + ' LIMIT :LimitSize OFFSET :OffsetSize' );
            prdc = results;  
        }
        else if (searchtext == null){
            List<Product__c> results = Database.query('SELECT Name, Id,  Price__c, Amount__c, Type__c, Added_Date__c,Release_Date__c, Available__c '+
                                                      'FROM Product__c'+' ORDER BY '+
                                                      sortOrder + ascOrDesc + ' LIMIT :LimitSize OFFSET :OffsetSize' );
            prdc = results;
        }
        return prdc;
    }
    
    public void searchName(){
        if(searchInput.length()>0){
            searchtext = searchInput;
        }
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR, 
                                                            ' Insert any letters please');
            ApexPages.addMessage(myMsg);
        }      
    }
    public void clearName(){
        searchText = null;
        searchInput = null;
    }
    
    public void insNew(){
        prod = new Product__c();
        rend = true;
        rend2 = false;
    }
    
    public void saveMe(){
        if(this.prod.Price__c <= 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR, 
                                                            ' Price must be more than 0 ');
            ApexPages.addMessage(myMsg);
        }
        else {
            if(prod.Added_Date__c == null){
                prod.Added_Date__c = system.now();
            }
            if(prod.Amount__c == null){
                prod.Amount__c = 0;
            }
            if(prod.Type__c == null){
                prod.Type__c = 'none';
            }
            insert prod;
            rend = false;
        }
    }
    public void sortByName() {
        this.sortOrder = 'Name';
        if(ascOrDesc == ' ASC ')
        {
            ascOrDesc = ' DESC ';
        }
        else
        {
            ascOrDesc = ' ASC ';
        }   
    }
    public void sortAdded_date() {
        this.sortOrder = 'Added_Date__c';
        if(ascOrDesc == ' ASC ')
        {
            ascOrDesc = ' DESC ';
        }
        else
        {
            ascOrDesc = ' ASC ';
        }   
    }
    public void sortRelease_date() {
        this.sortOrder = 'Release_Date__c';
        if(ascOrDesc == ' ASC ')
        {
            ascOrDesc = ' DESC ';
        }
        else
        {
            ascOrDesc = ' ASC ';
        }   
    }
    public void sortPrice() {
        this.sortOrder = 'Price__c';
        if(ascOrDesc == ' ASC ')
        {
            ascOrDesc = ' DESC ';
        }
        else
        {
            ascOrDesc = ' ASC ';
        }   
    }
    
    public void DeleteProduct() {
        prod = [SELECT Name, Price__c, Amount__c, Type__c,
                Added_Date__c, Release_Date__c, Available__c
                FROM Product__c where
                id=:plid];
        delete prod;
    }
    
    public void editProduct (){
        updatedProduct = [SELECT Name, Price__c, Amount__c, Type__c,
                          Added_Date__c, Release_Date__c, Available__c
                          FROM Product__c where
                          id=:plid]; 
        rend2 = true;
        rend = false;
    }
    
    public void updateME (){
        update updatedProduct;
        rend2 = false;
    }
    
    public void FirstPage(){
        OffsetSize = 0;
    }
    
    public void Previous(){
        OffsetSize = OffsetSize - LimitSize;
    }
    
    public void Next(){
        OffsetSize = OffsetSize + LimitSize;
    }
    
    public void LastPage(){
        OffsetSize = totalRecs - math.mod(totalRecs, LimitSize);
    }
    
    public boolean getprev(){
        if(OffsetSize == 0){
            return true;
        }
        else
            return false;
    }
    
    public boolean getnxt(){
        if((OffsetSize+LimitSize) >= TotalRecs){
            return true;
        }
        else
            return false;
    }
    
    public list<SelectOption> getPageSizeList(){
        list<SelectOption>  options = new list<SelectOption>();
        options.add(new selectOption('5','5'));
        options.add(new selectOption('10','10'));
        options.add(new selectOption('20','20'));
        return options;
    }
    
    public Integer LimitSizeL {
        get; set { // Выбираем количество записей на странице
            if(value != null) this.LimitSizeL = value;
            LimitSize = value;
        }
    }
}