public with sharing class ho {

    public static void MethodB(){
        Integer b = 3;
        System.debug(b);
    }



    public static void MethodA (){
        String a = 'GITGIT';
        System.debug('This is method A' + a);
    }
    

    public static String findFirstSingleChar(String str) {
        String letter;
        for (Integer i = 0; i < str.length(); i++) {
            Integer count = 0;
            for (Integer j = 0; j < str.length(); j++) {
                if (String.valueOf(str.charAt(i)) == String.valueOf(str.charAt(j)))
                    count++;
            }
            if (count == 1) {
                letter +='';
                letter += String.valueOf(str.charAt(i));
            }
        }
        return letter.left(1);
    }

    System.debug(LoopsTasks.findFirstSingleChar('The quick brown fox jumps over the lazy dog'));
}
