@isTest
private class TestProductTablePageController {
    @isTest static void TestButtonSave(){
        ApexPages.currentPage().getParameters().put('ProductController','?');
        ProductTablePageController MyPRController = new ProductTablePageController();
        MyPRController.insNew();
        Product__c c = MyPRController.prod;
        c.Name = 'Test';
        c.Price__c = 10;
        c.Release_Date__c = system.today()+5;
        c.Type__c = 'test type';
        c.Amount__c = 5;
        MyPRController.saveMe();
        Boolean a = MyPRController.rend;
        system.assert(!a);
        List<Product__c> getProducts_return = MyPRController.getProducts();
        for(Product__c prod : getProducts_return){
            system.assertEquals('Test', prod.Name);
            system.assert(prod.Available__c);
        }
        MyPRController.insNew();
        Product__c c1 = MyPRController.prod;
        c1.Name = 'Test';
        c1.Price__c = 0;
        c1.Release_Date__c = system.today()+5;
        c1.Type__c = 'test type';
        c1.Amount__c = 5;
        MyPRController.saveMe();
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertNotEquals(0, pageMessages.size());
    }
    @isTest static void TestButtonNew(){
        
        ApexPages.currentPage().getParameters().put('ProductController','?');
        ProductTablePageController MyPRController = new ProductTablePageController();
        MyPRController.insNew();
        Boolean b = MyPRController.rend;
        system.assert(b);
        Product__c c = MyPRController.prod;
        system.assert(c.Name == null);
    }
    @isTest static void TestButtonDelete(){
        
        ApexPages.currentPage().getParameters().put('ProductController','?');
        ProductTablePageController MyPRController = new ProductTablePageController();
        MyPRController.insNew();
        Product__c c = MyPRController.prod;
        c.Name = 'Test';
        c.Price__c = 10;
        c.Added_Date__c = system.today();
        c.Release_Date__c = system.today()+5;
        MyPRController.prod = c;
        MyPRController.saveMe();
        MyPRcontroller.plid=MyPRController.prod.id;
        MyPRController.DeleteProduct();
        System.assert(MyPRController.getProducts().size() == 0);

    }
@isTest static void TestButtonEdit(){
        
        ApexPages.currentPage().getParameters().put('ProductController','?');
        ProductTablePageController MyPRController = new ProductTablePageController();
        MyPRController.insNew();
        Product__c c = MyPRController.prod;
        c.Name = 'Test';
        c.Price__c = 10;
        c.Added_Date__c = system.today();
        c.Release_Date__c = system.today()+5;
        MyPRController.updatedproduct = c;
        MyPRController.saveMe();
        MyPRcontroller.plid=MyPRController.updatedproduct.id;
        MyPRController.editProduct();
        system.assert(MyPRController.updatedproduct.Name == 'Test');
        system.assert(MyPRController.rend2);
        system.assert(!MyPRController.rend);
        MyPRController.updatedproduct.Name = 'Test2';
        MyPRController.updateMe();
        system.assert(MyPRController.updatedproduct.Name == 'Test2');
        system.assert(!MyPRController.rend2);

    }
    
    @isTest static void TestButtonSearch(){
        
        ApexPages.currentPage().getParameters().put('ProductController','?');
        ProductTablePageController MyPRController = new ProductTablePageController();
        MyPRController.insNew();
        Product__c c = MyPRController.prod;
        c.Name = 'Test';
        c.Price__c = 10;
        c.Added_Date__c = system.today();
        c.Release_Date__c = system.today()+5;
        MyPRController.updatedproduct = c;
        MyPRController.saveMe();
        MyPRController.searchInput = 'te';
        MyPRController.searchname();
        system.assert( MyPRController.getProducts().size()==1);
        MyPRController.searchInput = '';
        MyPRController.searchname();
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertNotEquals(0, pageMessages.size());
        MyPRController.clearName();
        System.assert(MyPRController.searchInput == null) ;
        System.assert(MyPRController.searchText == null) ;
    }
    
    @isTest static void TestSortByName(){
        
        ApexPages.currentPage().getParameters().put('ProductController','?');
        ProductTablePageController MyPRController = new ProductTablePageController();
        MyPRController.sortByName();
        system.assert(MyPRController.sortOrder == 'Name');
        system.assert(MyPRController.ascOrDesc == ' DESC ');
        MyPRController.sortByName();
        system.assert(MyPRController.ascOrDesc == ' ASC ');
    }
    
    @isTest static void TestSortAddedDate(){
        
        ApexPages.currentPage().getParameters().put('ProductController','?');
        ProductTablePageController MyPRController = new ProductTablePageController();
        MyPRController.sortAdded_date();
        system.assert(MyPRController.sortOrder == 'Added_Date__c');
        system.assert(MyPRController.ascOrDesc == ' DESC ');
        MyPRController.sortAdded_date();
        system.assert(MyPRController.ascOrDesc == ' ASC ');
    }
    
    @isTest static void TestSortReleaseDate(){
        
        ApexPages.currentPage().getParameters().put('ProductController','?');
        ProductTablePageController MyPRController = new ProductTablePageController();
        MyPRController.sortRelease_date();
        system.assert(MyPRController.sortOrder == 'Release_Date__c');
        system.assert(MyPRController.ascOrDesc == ' DESC ');
        MyPRController.sortRelease_date();
        system.assert(MyPRController.ascOrDesc == ' ASC ');
    }
    
    @isTest static void TestSortPrice(){
        
        ApexPages.currentPage().getParameters().put('ProductController','?');
        ProductTablePageController MyPRController = new ProductTablePageController();
        MyPRController.sortPrice();
        system.assert(MyPRController.sortOrder == 'Price__c');
        system.assert(MyPRController.ascOrDesc == ' DESC ');
        MyPRController.sortPrice();
        system.assert(MyPRController.ascOrDesc == ' ASC ');
    }
    
}