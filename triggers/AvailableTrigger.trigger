trigger AvailableTrigger on Product__c (before insert, before update) {
    List <Product__c> prdct = Trigger.New;
    if(Trigger.isInsert || Trigger.isUpdate){
        for(Product__c c : prdct){
            if(c.Amount__c > 0){
            c.Available__c = true;
            }
            else {
            c.Available__c = false;
            }
        }
    }
}